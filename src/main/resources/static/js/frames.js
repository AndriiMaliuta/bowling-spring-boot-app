const hostUrl = () => {
    const protocol = location.protocol;
    const port = window.location.port;
    const host = window.location.hostname;
    return protocol.concat('//').concat(host).concat(':' + port);
};

let submitButton = document.getElementById('bowl-pins-submit');

submitButton.addEventListener('click', (e) => {
    let numberOfPins = document.querySelector('input[name="number-of-pins"]:checked').value;
    checkFinalAttempt();                                                                                       // Check final attempt
    postData(numberOfPins);
    console.log('Number of pins in input == ' + numberOfPins)
});

function nextAttempt() {
    axios.get(hostUrl() + "/rest/api/v1/attempts/next-attempt")
        .then(response => {
            console.log('nextAttempt Response is ' + typeof response.data + ' ' + response.data);
            return response.data;
        }).catch(error => console.log(error));
}

const postData = (numberOfPins) => {
    // let nextId = nextAttempt;
    axios.post(
        hostUrl() + "/rest/api/v1/attempts/",
        {
            // "id": 1,
            "numberOfPins": numberOfPins
        }
    ).then(response => {
        console.log(response.data);
        let attemptDivTemp = document.getElementById('attempt-div-' + response.data.attemptNumber);
        let frameDivTotal = document.getElementById('frame-total-div-' + response.data.bowlerFrame.frameNumber);
        attemptDivTemp.innerText = response.data.numberOfPins;
        frameDivTotal.innerText = response.data.bowlerFrame.frameNumberOfPins;
        getPinsTotalCount();

    })
        .catch(error => console.log(error));
};

const fetchData = (attempt) => {
    axios.get(hostUrl() + '/rest/api/v1/frames')
        .then(response => {
        }).catch(error => console.log(error));
};

const getPinsTotalCount = () => {
    const countData = document.getElementById('total-count-data');

    axios.get('http://localhost:8080/rest/api/v1/frames')
        .then(response => {
            let totalNumber = 0;
            for (let i = 0; i < response.data.length; i++) {
                totalNumber = totalNumber + response.data[i].frameNumberOfPins;
            }
            countData.innerText = totalNumber;

        }).catch(error => console.log(error));
};
const checkFinalAttempt = () => {
    let finalAttemptLocation = document.getElementById('attempt-div-20');
    if (finalAttemptLocation.innerText !== '') {
        location.href = "/stop-game";
    }
};
