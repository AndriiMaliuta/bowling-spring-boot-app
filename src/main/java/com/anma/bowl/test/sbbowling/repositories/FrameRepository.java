package com.anma.bowl.test.sbbowling.repositories;

import com.anma.bowl.test.sbbowling.models.BowlerFrame;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FrameRepository extends JpaRepository<BowlerFrame, Integer> {

}
