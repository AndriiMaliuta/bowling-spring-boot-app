package com.anma.bowl.test.sbbowling.rest.api.v1;


import com.anma.bowl.test.sbbowling.models.Attempt;
import com.anma.bowl.test.sbbowling.repositories.AttemptRepository;
import com.anma.bowl.test.sbbowling.services.AttemptsCreationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Slf4j
@RestController
@RequestMapping(AttemptsRestController.API_URL)
public class AttemptsRestController {

    public static final String API_URL = "/rest/api/v1/attempts";

    @Autowired
    private final AttemptRepository attemptRepository;

    @Autowired
    private final AttemptsCreationService attemptsCreationService;

    public AttemptsRestController(AttemptRepository attemptRepository,
                                  AttemptsCreationService attemptsCreationService) {
        this.attemptRepository = attemptRepository;
        this.attemptsCreationService = attemptsCreationService;
    }

    @GetMapping
    public ResponseEntity<Iterable<Attempt>> getAllAttempts() {

        return new ResponseEntity<>(attemptRepository.findAll(), HttpStatus.OK);
    }

    @GetMapping("/{attemptId}")
    public ResponseEntity<Attempt> getAttemptById(@PathVariable Integer attemptId) {

        return new ResponseEntity<>(attemptRepository.findById(attemptId).get(), HttpStatus.OK);
    }

    @PostMapping(consumes = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Attempt> createAttempt(@RequestBody @Valid Attempt attempt) {

        attemptsCreationService.createAssertion(attempt);

        log.info("****** POST successful. ID == " + attempt.getId().toString());

        return new ResponseEntity<>(attemptRepository.save(attempt), HttpStatus.CREATED);
    }

//    @PostMapping(path = "/{attemptId}", consumes = {MediaType.APPLICATION_JSON_VALUE})
//    public ResponseEntity<Attempt> updateFrame(@PathVariable String attemptId, @RequestBody @Valid Attempt newAttempt) {
//
//        Attempt existingAttempt = attemptRepository.findById(Integer.parseInt(attemptId)).get();
//
//
////        existingAttempt.setBowlerFrame();
//
//        return new ResponseEntity<>(attemptRepository.save(existingAttempt), HttpStatus.CREATED);
//    }

    @PutMapping(path = "/{attemptId}", consumes = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Attempt> updateFrame(@PathVariable String attemptId, @RequestBody @Valid Attempt newAttempt) {

        Attempt existingAttempt = attemptRepository.findById(Integer.parseInt(attemptId)).get();

        return new ResponseEntity<>(attemptRepository.save(existingAttempt), HttpStatus.CREATED);
    }

    @GetMapping("/next-attempt")
    public Integer getIdOfNextFrame() {

        int nextId = 0;

        return 2;                                            // TODO  HARDCODED !!
    }

}
