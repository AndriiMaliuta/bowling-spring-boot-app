package com.anma.bowl.test.sbbowling.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FrameData {

    private String numberOfPins;
    private String attemptType;
}
