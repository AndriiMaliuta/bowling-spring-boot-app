//package com.anma.bowl.test.sbbowling.config;
//
//import com.anma.bowl.test.sbbowling.models.BowlerFrame;
//import com.anma.bowl.test.sbbowling.repositories.FrameRepository;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.CommandLineRunner;
//import org.springframework.stereotype.Component;
//
//@Component
//@Slf4j
//public class Bootstrap implements CommandLineRunner {
//
//    @Autowired
//    private FrameRepository frameRepository;
//
//    @Override
//    public void run(String... args) throws Exception {
//        log.info("****** Loading Data ******");
//        loadData();
//    }
//
//    private void loadData() {
//
//        for (int i = 1; i < 11; i++) {
//            BowlerFrame bowlerFrame = BowlerFrame.builder()
//                    .id(i)
//                    .build();
//            frameRepository.save(bowlerFrame);
//            log.info("***** Created BowlerFrame with ID == " + bowlerFrame.getId());
//        }
//
//    }
//}
