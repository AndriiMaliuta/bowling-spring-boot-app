/*
@author - Andrii Maliuta
		  quadr988@gmail.com
 */
package com.anma.bowl.test.sbbowling;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SbBowlingApplication {

	public static void main(String[] args) {
		SpringApplication.run(SbBowlingApplication.class, args);
	}

}
