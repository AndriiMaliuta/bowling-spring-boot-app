package com.anma.bowl.test.sbbowling.services;

import com.anma.bowl.test.sbbowling.models.Attempt;
import com.anma.bowl.test.sbbowling.models.BowlerFrame;
import com.anma.bowl.test.sbbowling.repositories.AttemptRepository;
import com.anma.bowl.test.sbbowling.repositories.FrameRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AttemptsCreationServiceImpl implements AttemptsCreationService {

    @Autowired
    private final AttemptRepository attemptRepository;

    @Autowired
    private final FrameRepository frameRepository;

    public AttemptsCreationServiceImpl(AttemptRepository attemptRepository,
                                       FrameRepository frameRepository) {
        this.attemptRepository = attemptRepository;
        this.frameRepository = frameRepository;
    }

    @Override
    public void createAssertion(Attempt attempt) {

        switch (attemptRepository.findAll().size()) {
            case 0 :
                attempt.setAttemptNumber(1);
                BowlerFrame bowlerFrame1 = frameRepository.findAll().get(0);
                bowlerFrame1.setFrameNumberOfPins(attempt.getNumberOfPins());
                attempt.setBowlerFrame(bowlerFrame1);
                attemptRepository.save(attempt);
                break;
            case 1 :
                attempt.setAttemptNumber(2);
                BowlerFrame bowlerFrame2 = frameRepository.findAll().get(0);
                bowlerFrame2.setFrameNumberOfPins(attempt.getNumberOfPins() + attemptRepository.findAll().get(0).getNumberOfPins());
                attempt.setBowlerFrame(bowlerFrame2);
                attemptRepository.save(attempt);
                break;
            case 2 :
                attempt.setAttemptNumber(3);
                BowlerFrame bowlerFrame3 = frameRepository.findAll().get(1);
                bowlerFrame3.setFrameNumberOfPins(attempt.getNumberOfPins() );
                attempt.setBowlerFrame(bowlerFrame3);
                attemptRepository.save(attempt);
                break;
            case 3 :
                attempt.setAttemptNumber(4);
                BowlerFrame bowlerFrame4 = frameRepository.findAll().get(1);
                bowlerFrame4.setFrameNumberOfPins(attempt.getNumberOfPins() + attemptRepository.findAll().get(2).getNumberOfPins());
                attempt.setBowlerFrame(bowlerFrame4);
                attemptRepository.save(attempt);
                break;
            case 4 :
                attempt.setAttemptNumber(5);
                BowlerFrame bowlerFrame5 = frameRepository.findAll().get(2);
                bowlerFrame5.setFrameNumberOfPins(attempt.getNumberOfPins());
                attempt.setBowlerFrame(bowlerFrame5);
                attemptRepository.save(attempt);
                break;
            case 5 :
                attempt.setAttemptNumber(6);
                BowlerFrame bowlerFrame6 = frameRepository.findAll().get(2);
                bowlerFrame6.setFrameNumberOfPins(attempt.getNumberOfPins() + attemptRepository.findAll().get(4).getNumberOfPins());
                attempt.setBowlerFrame(bowlerFrame6);
                attemptRepository.save(attempt);
                break;
            case 6 :
                attempt.setAttemptNumber(7);
                BowlerFrame bowlerFrame7 = frameRepository.findAll().get(3);
                bowlerFrame7.setFrameNumberOfPins(attempt.getNumberOfPins() );
                attempt.setBowlerFrame(bowlerFrame7);
                attemptRepository.save(attempt);
                break;
            case 7 :  attempt.setAttemptNumber(8);
                BowlerFrame bowlerFrame8 = frameRepository.findAll().get(3);
                bowlerFrame8.setFrameNumberOfPins(attempt.getNumberOfPins() + attemptRepository.findAll().get(6).getNumberOfPins());
                attempt.setBowlerFrame(bowlerFrame8);
                attemptRepository.save(attempt);
                break;
            case 8 :  attempt.setAttemptNumber(9);
                BowlerFrame bowlerFrame9 = frameRepository.findAll().get(4);
                bowlerFrame9.setFrameNumberOfPins(attempt.getNumberOfPins() );
                attempt.setBowlerFrame(bowlerFrame9);
                attemptRepository.save(attempt);
                break;
            case 9 :  attempt.setAttemptNumber(10);
                BowlerFrame bowlerFrame10 = frameRepository.findAll().get(4);
                bowlerFrame10.setFrameNumberOfPins(attempt.getNumberOfPins() + attemptRepository.findAll().get(8).getNumberOfPins());
                attempt.setBowlerFrame(bowlerFrame10);
                attemptRepository.save(attempt);
                break;
            case 10 :  attempt.setAttemptNumber(11);
                BowlerFrame bowlerFrame11 = frameRepository.findAll().get(5);
                bowlerFrame11.setFrameNumberOfPins(attempt.getNumberOfPins() );
                attempt.setBowlerFrame(bowlerFrame11);
                attemptRepository.save(attempt);
                break;
            case 11 :  attempt.setAttemptNumber(12);
                BowlerFrame bowlerFrame12 = frameRepository.findAll().get(5);
                bowlerFrame12.setFrameNumberOfPins(attempt.getNumberOfPins() + attemptRepository.findAll().get(10).getNumberOfPins());
                attempt.setBowlerFrame(bowlerFrame12);
                attemptRepository.save(attempt);
                break;
            case 12 :  attempt.setAttemptNumber(13);
                BowlerFrame bowlerFrame13 = frameRepository.findAll().get(6);
                bowlerFrame13.setFrameNumberOfPins(attempt.getNumberOfPins() );
                attempt.setBowlerFrame(bowlerFrame13);
                attemptRepository.save(attempt);
                break;
            case 13 :  attempt.setAttemptNumber(14);
                BowlerFrame bowlerFrame14 = frameRepository.findAll().get(6);
                bowlerFrame14.setFrameNumberOfPins(attempt.getNumberOfPins() + attemptRepository.findAll().get(12).getNumberOfPins());
                attempt.setBowlerFrame(bowlerFrame14);
                attemptRepository.save(attempt);
                break;
            case 14 :  attempt.setAttemptNumber(15);
                BowlerFrame bowlerFrame15 = frameRepository.findAll().get(7);
                bowlerFrame15.setFrameNumberOfPins(attempt.getNumberOfPins() );
                attempt.setBowlerFrame(bowlerFrame15);
                attemptRepository.save(attempt);
                break;
            case 15 :  attempt.setAttemptNumber(16);
                BowlerFrame bowlerFrame16 = frameRepository.findAll().get(7);
                bowlerFrame16.setFrameNumberOfPins(attempt.getNumberOfPins() + attemptRepository.findAll().get(14).getNumberOfPins());
                attempt.setBowlerFrame(bowlerFrame16);
                attemptRepository.save(attempt);
                break;
            case 16 :  attempt.setAttemptNumber(17);
                BowlerFrame bowlerFrame17 = frameRepository.findAll().get(8);
                bowlerFrame17.setFrameNumberOfPins(attempt.getNumberOfPins() );
                attempt.setBowlerFrame(bowlerFrame17);
                attemptRepository.save(attempt);
                break;
            case 17 :  attempt.setAttemptNumber(18);
                BowlerFrame bowlerFrame18 = frameRepository.findAll().get(8);
                bowlerFrame18.setFrameNumberOfPins(attempt.getNumberOfPins() + attemptRepository.findAll().get(16).getNumberOfPins());
                attempt.setBowlerFrame(bowlerFrame18);
                attemptRepository.save(attempt);
                break;
            case 18 :  attempt.setAttemptNumber(19);
                BowlerFrame bowlerFrame19 = frameRepository.findAll().get(9);
                bowlerFrame19.setFrameNumberOfPins(attempt.getNumberOfPins() );
                attempt.setBowlerFrame(bowlerFrame19);
                attemptRepository.save(attempt);
                break;
            case 19 :  attempt.setAttemptNumber(20);
                BowlerFrame bowlerFrame20 = frameRepository.findAll().get(9);
                bowlerFrame20.setFrameNumberOfPins(attempt.getNumberOfPins() + attemptRepository.findAll().get(18).getNumberOfPins());
                attempt.setBowlerFrame(bowlerFrame20);
                attemptRepository.save(attempt);
                break;
        }

    }
}
