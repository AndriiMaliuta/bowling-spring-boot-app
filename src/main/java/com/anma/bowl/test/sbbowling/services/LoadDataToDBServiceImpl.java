package com.anma.bowl.test.sbbowling.services;

import com.anma.bowl.test.sbbowling.models.BowlerFrame;
import com.anma.bowl.test.sbbowling.repositories.FrameRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class LoadDataToDBServiceImpl implements LoadDataToDBService {

    @Autowired
    private final FrameRepository frameRepository;

    public LoadDataToDBServiceImpl(FrameRepository frameRepository) {
        this.frameRepository = frameRepository;
    }

    @Override
    public void loadDataToDB() {

        log.info("****** Loading Data ******");

        for (int i = 1; i < 11; i++) {
            BowlerFrame bowlerFrame = BowlerFrame.builder()
                    .id(i)
                    .frameNumber(i)
                    .build();
            frameRepository.save(bowlerFrame);

            log.info("***** Created BowlerFrame with ID == " + bowlerFrame.getId());
        }

    }
}
