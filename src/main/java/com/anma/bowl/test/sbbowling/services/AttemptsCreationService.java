package com.anma.bowl.test.sbbowling.services;

import com.anma.bowl.test.sbbowling.models.Attempt;

public interface AttemptsCreationService {

    void createAssertion(Attempt attempt);
}
