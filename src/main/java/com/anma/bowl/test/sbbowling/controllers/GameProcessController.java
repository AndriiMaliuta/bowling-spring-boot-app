package com.anma.bowl.test.sbbowling.controllers;

import com.anma.bowl.test.sbbowling.models.BowlerFrame;
import com.anma.bowl.test.sbbowling.models.FrameData;
import com.anma.bowl.test.sbbowling.repositories.FrameRepository;
import com.anma.bowl.test.sbbowling.services.EmptyDBService;
import com.anma.bowl.test.sbbowling.services.LoadDataToDBService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;

@Controller
@Slf4j
public class GameProcessController {

    @Autowired
    private final FrameRepository frameRepository;

    @Autowired
    private final EmptyDBService emptyDBService;

    @Autowired
    private final LoadDataToDBService loadDataToDBService;

    public GameProcessController(FrameRepository frameRepository,
                                 EmptyDBService emptyDBService,
                                 LoadDataToDBService loadDataToDBService) {
        this.frameRepository = frameRepository;
        this.emptyDBService = emptyDBService;
        this.loadDataToDBService = loadDataToDBService;
    }

    @GetMapping("/start-game")
    public String getFramesViewPage(HttpServletRequest httpRequest, Model model) {

        emptyDBService.emptyDB();
        loadDataToDBService.loadDataToDB();

        model.addAttribute("frameData", new FrameData());
        model.addAttribute("frames", frameRepository.findAll());
        model.addAttribute("url", httpRequest.getRequestURL());
        model.addAttribute("uri", httpRequest.getRequestURI());

        return "frames";
    }

    @GetMapping("/stop-game")
    public String endGame(Model model) {

        int score = 0;

        for (BowlerFrame frame : frameRepository.findAll()) {
            score = score + frame.getFrameNumberOfPins();
        }

        model.addAttribute("score", score);

        return "/score-page";
    }

}
