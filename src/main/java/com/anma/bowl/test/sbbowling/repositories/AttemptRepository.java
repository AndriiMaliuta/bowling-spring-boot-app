package com.anma.bowl.test.sbbowling.repositories;

import com.anma.bowl.test.sbbowling.models.Attempt;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AttemptRepository extends JpaRepository<Attempt, Integer> {
}
