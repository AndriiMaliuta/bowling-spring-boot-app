package com.anma.bowl.test.sbbowling.config;

import com.anma.bowl.test.sbbowling.exceptions.ErrorMessage;
import com.anma.bowl.test.sbbowling.exceptions.FrameNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.Date;

@ControllerAdvice
public class Exceptionshandler {

    @ResponseBody
    @ExceptionHandler(value = {FrameNotFoundException.class})
    @ResponseStatus(HttpStatus.NOT_FOUND)
    String blogNotFoundHandler(FrameNotFoundException e) {

        ErrorMessage errorMessage = new ErrorMessage(new Date(), e.getMessage());

        return errorMessage.toString();
    }

}
