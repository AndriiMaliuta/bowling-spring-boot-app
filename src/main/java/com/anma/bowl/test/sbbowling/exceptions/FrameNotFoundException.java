package com.anma.bowl.test.sbbowling.exceptions;

public class FrameNotFoundException extends RuntimeException {

    private static final long serialVersionUID = 5607629723563876825L;

    public FrameNotFoundException(String message) {
        super(message);
    }
}
