package com.anma.bowl.test.sbbowling.services;

import com.anma.bowl.test.sbbowling.repositories.AttemptRepository;
import com.anma.bowl.test.sbbowling.repositories.FrameRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class EmptyDBServiceImpl implements EmptyDBService {

    @Autowired
    private final FrameRepository frameRepository;

    @Autowired
    private final AttemptRepository attemptRepository;

    public EmptyDBServiceImpl(FrameRepository frameRepository,
                              AttemptRepository attemptRepository) {
        this.frameRepository = frameRepository;
        this.attemptRepository = attemptRepository;
    }

    @Override
    public void emptyDB() {

        log.info("***** Deleting data from DB");

        attemptRepository.deleteAll();
        frameRepository.deleteAll();

        log.info("***** number of Frames == " + frameRepository.findAll());
        log.debug("***** number of Attempts == " + attemptRepository.findAll());
    }
}
