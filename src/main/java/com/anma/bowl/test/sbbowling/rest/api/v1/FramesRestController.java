package com.anma.bowl.test.sbbowling.rest.api.v1;

import com.anma.bowl.test.sbbowling.models.BowlerFrame;
import com.anma.bowl.test.sbbowling.repositories.FrameRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Slf4j
@CrossOrigin(origins = "http://localhost:8080")
@RestController
@RequestMapping(FramesRestController.API_URL)
public class FramesRestController {

    public static final String API_URL = "/rest/api/v1/frames";

    @Autowired
    private FrameRepository frameRepository;


    @GetMapping(produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Iterable<BowlerFrame>> getAllFrames() {

        return new ResponseEntity<>(frameRepository.findAll(), HttpStatus.OK);
    }

    @GetMapping(path = "/{frameId}", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<BowlerFrame> getFrame(@PathVariable Integer frameId) {

        return new ResponseEntity<BowlerFrame>(frameRepository.findById(frameId).get(), HttpStatus.OK);
    }

    @PostMapping(consumes = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<BowlerFrame> createFrame(@RequestBody @Valid BowlerFrame bowlerFrame) {

        log.info("****** POST successful ******");
//        log.info(bowlerFrame.getId().toString());

        return new ResponseEntity<>(frameRepository.save(bowlerFrame), HttpStatus.CREATED);
    }

    @PutMapping(path = "/{frameId}", consumes = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<BowlerFrame> changeFrame(@PathVariable Integer frameId, @RequestBody @Valid BowlerFrame newBowlerFrame) {

        BowlerFrame existingBowlerFrame = frameRepository.findById(frameId).get();

//        if (existingBowlerFrame.getAttempts().size() == 0) {
//            Attempt attempt1 = new Attempt();
//            attempt1.setBowlerFrame(existingBowlerFrame);
//            attempt1.setNumberOfPins();
//            attempt1.setFoul();
//            attempt1.setNext();
//        }

//        existingBowlerFrame.setId(frameId);
//        existingBowlerFrame.setAttempts();
        existingBowlerFrame.setFrameNumberOfPins(newBowlerFrame.getFrameNumberOfPins());

        return new ResponseEntity<>(frameRepository.save(existingBowlerFrame), HttpStatus.CREATED);
    }


}
