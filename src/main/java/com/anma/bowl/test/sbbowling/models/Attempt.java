package com.anma.bowl.test.sbbowling.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Attempt {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "attempt_id")
    private Integer id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "frame_id", referencedColumnName = "frame_id")
    private BowlerFrame bowlerFrame;

    private int attemptNumber;
    private int numberOfPins;
    private boolean isNext;
    private boolean isFoul;
    private boolean isSpare;
    private boolean isStrike;


}
