package com.anma.bowl.test.sbbowling.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "frames")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class BowlerFrame {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "frame_id")
    private Integer id;

    @JsonIgnore
    @OneToMany(mappedBy = "bowlerFrame")
    private List<Attempt> attempts;

    private int frameNumber;
    private int frameNumberOfPins;


}
