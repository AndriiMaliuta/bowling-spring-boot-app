package com.anma.bowl.test.sbbowling.controllers;

import com.anma.bowl.test.sbbowling.models.BowlerFrame;
import com.anma.bowl.test.sbbowling.repositories.FrameRepository;
import com.anma.bowl.test.sbbowling.services.EmptyDBService;
import com.anma.bowl.test.sbbowling.services.LoadDataToDBService;

import org.junit.Before;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;
import static org.assertj.core.api.Assertions.assertThat;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@WebMvcTest(controllers = {GameProcessController.class})
class GameProcessControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private FrameRepository frameRepository;

    @Autowired
    private EmptyDBService emptyDBService;

    @Autowired
    private LoadDataToDBService loadDataToDBService;

    @Before
    void setUp() {
        BowlerFrame bowlerFrame1 = BowlerFrame.builder()
                .id(1)
                .frameNumber(1)
                .frameNumberOfPins(6)
                .build();
        frameRepository.save(bowlerFrame1);
        BowlerFrame bowlerFrame2 = BowlerFrame.builder()
                .id(2)
                .frameNumber(2)
                .frameNumberOfPins(7)
                .build();
        frameRepository.save(bowlerFrame2);
    }

    @Test
    void testFramesNumber() {
        assertThat(this.frameRepository).isNotNull();
    }


    @Test
    void isStatusOk() throws Exception {
        this.mockMvc.perform(get("/start-game"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("Number of pins:")));
    }

    @Test
    void endGame() {

    }
}